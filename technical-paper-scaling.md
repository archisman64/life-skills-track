# Scaling to Improve Performance

**Scalability** is the property of a system to handle a growing amount of work.

We can acheive this in two ways:
- **Horizontal Scaling**:
    Scaling horizontally (out/in) means adding more nodes to (or removing nodes from) a system, such as adding a new computer to a distributed software application. 
- **Vertical Scaling**:
    Scaling vertically (up/down) means adding resources to (or removing resources from) a single node, typically involving the addition of CPUs, memory or storage to a single computer.
---
## Load Balancing
Load balancing is the method of distributing network traffic equally across a pool of resources that support an application. **Load balancing falls under horizontal scaling.**
### **Benefits of Load Balancing**:
**Application availability**:
    Load balancers increase the fault tolerance of your systems by automatically detecting server problems and redirecting client traffic to available servers. You can use load balancing to make these tasks easier:

- Run application server maintenance or upgrades without application downtime
- Provide automatic disaster recovery to backup sites
- Perform health checks and prevent issues that can cause downtime

**Application scalability**:
    Your applications can handle thousands of client requests because load balancing does the following:

- Prevents traffic bottlenecks at any one server
- Predicts application traffic so that you can add or remove different servers, if needed
- Adds redundancy to your system so that you can scale with confidence

**Application security**:
    Load balancers come with built-in security features to add another layer of security to your internet applications. Load balancers can also do the following:

- Monitor traffic and block malicious content
- Automatically redirect attack traffic to multiple backend servers to minimize impact
- Route traffic through a group of network firewalls for additional security

**Application performance**: 
    Load balancers improve application performance by increasing response time and reducing network latency by the following means:

- Distributing the load evenly between servers to improve application      performance
- Redirecting client requests to a geographically closer server to reduce latency
- Ensuring the reliability and performance of physical and virtual computing resources
---
#### Take a look at the below diagram:
![load balancing diagram](https://www.nginx.com/wp-content/uploads/2014/07/what-is-load-balancing-diagram-NGINX.png)


What we have done here is increased the number of servers (**horizontal scaling**) that you have so that the increased traffic can be served without any problem and connect them to your load balancer. You then take the IP address of the server where load balancer is hosted and map it to the domain of your website. The request now comes to the load balancer and it evenly distribute the requests to the application server.


---
### **Load Balancing Algorithms**
Different load balancing algorithms provide different benefits; the choice of load balancing method depends on your needs:

- **Round Robin**: Requests are distributed across the group of servers sequentially.
- **Least Connections**: A new request is sent to the server with the fewest current connections to clients. The relative computing capacity of each server is factored into determining which one has the least connections.
- **Least Time**: Sends requests to the server selected by a formula that combines the fastest response time and fewest active connections. Exclusive to NGINX Plus.
- **Hash**: Distributes requests based on a key you define, such as the client IP address or the request URL. NGINX Plus can optionally apply a consistent hash to minimize redistribution of loads if the set of upstream servers changes.
- **IP Hash**: The IP address of the client is used to determine which server receives the request.
- **Random with Two Choices**: Picks two servers at random and sends the request to the one that is selected by then applying the Least Connections algorithm (or for NGINX Plus the Least Time algorithm, if so configured).
---
## Conclusion
**Load balancing plays a critical role in scaling systems horizontally**, providing improved performance, scalability, and fault tolerance. Understanding load balancing principles and selecting suitable strategies are key considerations for organizations aiming to build robust, scalable, and highly available architectures. 

---
## References
1. https://en.wikipedia.org/wiki/Scalability
1. https://aws.amazon.com/what-is/load-balancing/
1. https://www.nginx.com/resources/glossary/load-balancing/