## Steps/Strategies to do Active Listening
1. Avoid getting distracted by your own thoughts.
1. Try not to interrupt the other person.
1. Show interest in the topic that the speaker is talking about.
1. Use gestures to show that you are listening to them.
1. You may keep notes in case of an important meeting or discussion.
1. Paraphrase sometimes to make sure both the speaker and you are in the same page.

## Key points of Reflective Listening according to Fisher's Model
- Give your full attention to the speaker and focus on what they are saying.
- Maintain eye contact, nod occasionally, and use facial expressions to convey understanding.
- After the speaker has finished expressing their thoughts, paraphrase or restate what they said in your own words. 
- Try to understand and acknowledge the speaker's emotions and feelings.
- Suspend judgment and refrain from offering immediate advice, criticism, or personal opinions. 
- Periodically summarize what the speaker has said to ensure that you have captured the main points accurately. 

---
## My Reflectios:
**Obstacles in My Listening process**
I believe I am a good listener but there are a few areas where I think I can improve:
- Can't listen for long durations without break.
- The topic has to be of my interest otherwise I can't pay attention.

**How I can improve**
- Pay attention to nonverbal signs, such as body language and tone.
- Make a mental image of what the speaker is saying.
- Keep an open mind.
- Consider eye contact.

### When do you switch to Passive communication style in your day to day life?
Switching to passive communication style in day-to-day life can happen in situations where a person avoids expressing their thoughts, needs, or desires. This may occur when someone lacks confidence, fears conflict, or wants to maintain harmony at all costs. 
It can be seen when someone consistently puts others' needs above their own, avoids confrontation, and struggles to assert themselves in various interpersonal interactions.

### When do you switch into Aggressive communication styles in your day to day life?
Switching to aggressive communication style in day-to-day life can occur when someone expresses their thoughts, needs, or desires in a forceful and disrespectful manner. People may resort to aggressive communication when they feel threatened, frustrated, or want to control a situation.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
Switching to passive-aggressive communication style in day-to-day life happens when someone indirectly expresses their negative feelings or intentions. It involves using sarcasm, gossiping, giving the silent treatment, or making subtle taunts instead of openly addressing conflicts or issues. Passive-aggressive behavior often stems from a fear of confrontation or a desire to retaliate indirectly.

### How can you make your communication assertive?
1. Be self-aware: Understand your own thoughts, feelings, and needs before communicating with others. Reflect on what you want to express and why.
1. Use "I" statements: Clearly and directly express your thoughts and feelings using "I" statements to take ownership of your emotions.
1. Active listening: Pay attention to others, show genuine interest, and seek to understand their perspectives.
1. Respectful assertiveness: Express your opinions and needs in a confident and respectful manner. 
1. Set clear boundaries: Clearly communicate your boundaries and expectations to others. Let them know what is acceptable to you and what is not, and be willing to negotiate and find compromises when needed.
1. Conflict resolution: When conflicts arise, address them openly and assertively. Use problem-solving approaches, actively listen to others' viewpoints, and collaborate to find mutually beneficial solutions.