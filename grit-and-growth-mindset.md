# Grit and Growth Mindset

## Grit
The video by Angela Lee Duckworth focuses on the imprtance of preservence and passion for long term success.
She starts with sharing her personal experience as a math teacher where she noticed that even students with lower IQs were excelling in tests and assignments which led her to do research on what factors beyond intelligence contribute to achievement.
She highlights her research findings that show grit is crucial in various contexts, such as academic settings, military training, and competitive sports. She emphasizes the importance of cultivating a growth mindset, which is the belief that abilities and intelligence can be developed through effort and practice.
In conclusion, she advocates for a shift in our educational system's focus from solely measuring intelligence and talent to valuing and nurturing the development of grit. 

## My takeaways from the video-
- to believe that your abilities and intelligence can improve through effort and practice. Adopting a growth mindset will allow me to see challenges as opportunities for growth and learning.
- to identify my interests and passions and align them with my long-term goals. Find activities and pursuits that genuinely excite and motivate me, as passion provides the fuel to sustain your efforts.
- to keep going even when faced with difficulties. View failures as learning opportunities and keep pushing forward.
- to recognize the importance of grit as a valuable trait for success. Make a conscious effort to prioritize grit in my life by consistently demonstrating perseverance, passion, and a growth mindset.

## Introduction to Growth Mindset
The video serves as an introduction to the concept of a growth mindset, explaining its importance in approaching challenges, embracing learning and development, and ultimately achieving success. It starts by contrasting two mindsets: a fixed mindset and a growth mindset.
A fixed mindset is characterized by the belief that intelligence and abilities are fixed traits that cannot be changed. On the other hand, a growth mindset is a belief that intelligence and abilities can be developed and improved through effort, practice, and learning.
It highlights the role of effort and persistence in the growth mindset. It emphasizes that people with a growth mindset understand that success is not solely dependent on innate talent but also on putting in consistent effort, seeking feedback, and learning from mistakes.
By consciously adopting a growth mindset and implementing strategies like embracing challenges, seeking feedback, and persevering in the face of obstacles, individuals can gradually shift their mindset and unlock their true potential.

**Following are my takeaways from the video:**
- Embrace the belief that intelligence and abilities can be developed through effort and practice.
- See challenges as opportunities for growth and learning rather than as threats or indicators of failure.
- Put in consistent effort, seek feedback, and learn from mistakes to improve and grow.
- Use positive self-talk and affirmations to cultivate a belief in your own potential for growth.
- Surround yourself with a supportive environment that encourages and nurtures a growth mindset.

## Internal Locus of Control

Locus of control refers to the belief of whether we have control over the outcomes of our lives or if external forces dictate our success.
**Internal locus of control** refers to the belief that individuals have control over their own lives and the outcomes they experience. It is the perception that one's actions, decisions, and efforts significantly influence the results they achieve.