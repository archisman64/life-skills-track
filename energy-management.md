# Energy Management

## What are the activities you do that make you relax - Calm quadrant?
I like to exercise to keep myself calm and manage my energy. To be specific I swim, bike and run regularyly and it definitely helps me stay calm.

## When do you find getting into the Stress quadrant?
I find myself in stress quadrant in the following situations:
- I am alone and don't have much work to do.
- I had some financial loss.
- I have to do something that I don't like doing.
- Something bad happened with me or with someone whom I care about.

## How do you understand if you are in the Excitement quadrant?
I feel the following changes when I am excited:
- butterflies in my stomach.
- feeling joy or happiness thinking about some event.
- I suddenly get a boost of energy even if I was feeling lathergic just some time before.
- often smile within myself.
- have difficulty focusing on other tasks in hand.

## Sleep is your Superpower | Matt Walker
Matt Walker explains that sleep is a crucial aspect of our health. Getting enough good-quality sleep is like having a superpower because it benefits our physical and mental well-being. 
He also warns about the harmful effects of sleep deprivation on our health, performance, and safety.
To improve our lives, Walker encourages us to prioritize and value sleep as an essential part of our daily routine.

## What are some ideas that you can implement to sleep better?
I can think of the following things to make my sleep better:
- reduce exposure to screen lights after evening and completely stop it atleast an hour before going to bed.
- going to a **short walk** in the park/nature and trying to distract myself from the stresses of professional life.
- taking a warm bath before bed also helps me reduce some stress.
- limiting the intake of caffeine and alcohol close to bedtime.
- indulge in some sort of physical activity everyday but not too close to bedtime.

## Brain Changing Benefits of Exercise


## What are some steps you can take to exercise more?
I think I already exercise a lot :P and I would love to be able to do more because that's my passion (Triathlon). 
Currently, I can do the following things:
- go to bed early.
- wake up early and workout an hour.
- maybe take a nap before going to office.
- maintain a healthy/nutritious diet.
- squeeze in another short workout after reaching home in the evening. 
(Actually that's pretty much what I am trying to do :p.)
