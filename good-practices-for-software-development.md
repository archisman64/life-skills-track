# Good Practices for Software Development

## My takeaways from each section:
1. Take note of the requirements properly and seek frequent feedback about the work in progress.
1. Always keep the concerned team and managers notified about any unexpected issues or deadline changes.
1. Don't hesitate to ask questions or seek help when stuck but do it in a responsible manner keeping in mind the other person's availability.
1. Make some effort to communicate with your team members and do so with their schedules in mind as well.
1. Don't bombard anyone with messages and professionally ask questions with a proper description of the issue.
1. Work with full concentration or don't. Use the deep-work principle while working and take a break when feeling tired.

## My area of imporovement:
- I need to work on my ability to concentrate on work for long durations. I am good at working deeply for short durations. I want to be able to concentrate for longer (like an hour or more).
- I would try to improve on the above-said area by building over the work durations slowly and trying to implement deep work.
- I would also like to reduce my time indulging in social media. I would do so by applying the suggestions given in the previous tasks and being mindful of the actual necessity of consuming some content on any social media platform.