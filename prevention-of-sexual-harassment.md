# Prevention of Sexual Harassment
## What kinds of behaviour cause sexual harassment?

Any kind of unwelcomed behaviour can cause sexual harassment. It could be verbal, visual or physical in nature.

- **Verbal**: This could be unwanted comments regarding someone's clothing, body or gender remarks. Requesting sexual favours or asking out repeatedly.

- **Visual**: Poster, drawing, pictures, screensavers, emails or texts of sexual nature can be counted as sexual harassment.

- **Physical**: Sexual assault, impeding or blocking movement, inappropriate touching such as kissing, hugging, patting, stroking, or rubbing, sexual gesturing, or even leering or staring.
---
## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

I would try to keep some kind of proof of the incident and report it to any member of the 'Internal Complaint Committee' as instructed in the Employee Handbook.